#!/bin/bash

BASE_API_URL="https://gitlab.com/api/v4/projects/"
API_TOKEN=$(cut /etc/deferred_gitlab -f2 -d ':')
PROJECT_ID=$(cat .git/config  | grep gitlab  | cut -f2 -d ':' | sed 's/\//%2F/;s/\.git//')

OK="$(tput setaf 2)[ OK ]$(tput sgr0)"
FAIL="$(tput setaf 1)[FAIL]$(tput sgr0)"


if [[ $1 == "createissue" ]] || [[ $1 == "ci" ]]
then
	echo "Create issue"
	echo "============"
	echo ""
	printf "Enter a title: "
	read title
	
	if [[ $title == "" ]]
	then
		echo "'title' is a mandatory field. Abort"
		exit 1
	fi
	
	
	printf "Enter an additional description: "
	read desc
	printf "Comma separated list of labels: "
	read labels


	mkdir -p .git/deferred_gitlab/
	FILE=$(mktemp | cut -f2 -d '.')".issue"
	
	echo "$title"  > ".git/deferred_gitlab/$FILE"
	echo "$labels"  >> ".git/deferred_gitlab/$FILE"
	echo "$desc"  >> ".git/deferred_gitlab/$FILE"
	
	echo "Entry '$title' saved and ready to be pushed!"

else if [[ $1 == "push" ]]
then
	FILES=$(ls .git/deferred_gitlab/*.issue 2>/dev/null)
	if [[ $? != 0 ]]
	then
		echo "No issues found! :)"
		exit 0
	fi

	for FILE in $FILES
	do
		title=$(head -n1 $FILE)
		labels=$(head -n2 $FILE | tail -n1)
		desc=$(head -n3 $FILE | tail -n1)
		
		if [[ $labels != "" ]]
		then
			LABELS_PARAM="labels=$labels" 
		fi
		
		if [[ $DESC_PARAM != "" ]]
		then
			DESC_PARAM="description=$desc"
		fi
		
		URL="$BASE_API_URL$PROJECT_ID/issues/"
		printf "Pushing '$title' to GitLab"
		curl --request POST --header "PRIVATE-TOKEN: $API_TOKEN" --data-urlencode "title=$title" --data-urlencode $DESC_PARAM --data-urlencode $LABELS_PARAMS "$URL" >/dev/null 2>&1
		if [[ $? == 0 ]]
		then
			printf "%*s%s\n" $(($(tput cols)-10-${#title}))  "$OK"
			rm $FILE
		else
			printf "%*s%s\n" $(($(tput cols)-10-${#title}))  "$FAIL"
		fi
	done
else
	echo "Please specify a command:"
	echo "createissue, push"
fi
fi
